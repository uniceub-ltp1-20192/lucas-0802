from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.entidadeFilha import Cliente

class Menu:
 
    @staticmethod
    def menuPrincipal():
        print("""
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar
""")
        return Validador.validarOpcaoMenu("[0-4]")
    @staticmethod
    def menuConsultar():
        print("""
0 - Voltar
1 - Consultar por Identificador
2 - Consultar por Propriedade
""")
        return Validador.validarOpcaoMenu("[0-2]")

    @staticmethod
    def iniciarMenu():
        d = Dados()
        opMenu = ""
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                print("entrou em Consultar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if(retorno != None):
                            print(retorno)   
                        else:
                            print("""
                            Nao foi encontrado 
                            nenhum registro com
                            esse identificador
                            """)                
                        
                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if(retorno != None):
                            print(retorno)   
                        else:
                            print("""
                            Nao foi encontrado 
                            nenhum registro com
                            esse Atributo
                            """)                
                        
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
               
            elif opMenu == "2":
                print("entrou em Inserir")
                Menu.menuInserir(d)

            elif opMenu == "3":
                print("entrou em alterar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                                      
                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                              
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "4":
                print("entrou em deletar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)
                    elif opMenu == "2":
                        retorno = Menu.menuBuscaPorAtributo(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)

                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "0":
                print("Saindo")
            elif opMenu == "":
                print("")
            else:
                print("Informe uma opcao valida")
  
  
    @staticmethod
    def menuBuscaPorIdentificador(d):
        retorno =  d.buscarIdentificador(Validador.verificarInteiro())
        return retorno

  
    @staticmethod
    def menuBuscaPorAtributo(d :Dados):
        retorno = d.buscarPorAtributo(input("Informe um senha para busca:"))
        return retorno

    @staticmethod
    def menuInserir(d):
        c = Cliente()
        c.nome = input("informe nome:")
        c.idade = input("infome idade:")
        c.cpf = input("informe CPF:")
        c.nacionalidade = input("infome Nacinalidade:")
        c.usuario= input("informe usuario:")
        c.senha = input("Informe senha:")
        d.inserir(c)         
   
   
    @staticmethod
    def menuAlterar(retorno,d):
        
        retorno.nome = Validador.criarValidadorTest1(retorno.nome,"informe nome:")
        retorno.idade = Validador.criarValidadorTest1(retorno.idade,"informe idade:")
        retorno.cpf = Validador.criarValidadorTest1(retorno.cpf,"informe CPF:")
        retorno.nacionalidade = Validador.criarValidadorTest1(retorno.nacionalidade,"informe nacionalidade:")
        retorno.usuario= Validador.criarValidadorTest1(retorno.usuario,"informe usuario:")
        retorno.senha = Validador.criarValidadorTest1(retorno.senha,"informe senha:")
        d.alterar(retorno)

    def menuDeletar(retorno,d):
        print(retorno)
        deletar = input("""
        Deseja deletar o registro:
        S - Sim
        """)
        if deletar == "S" or deletar == "s":
            d.deletar(retorno)
            print("""
            Registro deletado
            """)
        else:
            print("""
            Registro nao foi deletado
            """)

 